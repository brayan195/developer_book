import Vue from 'vue';
import Router from 'vue-router';
import Login from './views/login.vue';
import Home from './views/home.vue';

import Developers from './components/Developers/app_add_developer.vue';
import GetDevelopers from './components/Developers/app_developer.vue';
import EditDevelopers from './components/Developers/app_edit_developer.vue';


import Empresa from './components/Empresa/app_add_Empresa.vue';
import GetEmpresa from './components/Empresa/app_Empresa.vue';
import EditEmpresa from './components/Empresa/app_edit_Empresa.vue';






import App from './App.vue';
import VueLocalStorage from 'vue-localstorage';

Vue.use(VueLocalStorage);
Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'login',
      component: Home
    },
    {
      path: '/home',
      name: 'home',
      component: Home,


        children: [
          {path: '/Developers', name: 'Developers', component: Developers},
          {path: '/GetDevelopers', name: 'GetDevelopers', component: GetDevelopers},
          {path: '/EditDevelopers/:id', name: 'EditDevelopers', component: EditDevelopers},

          {path: '/Empresa', name: 'Empresa', component: Empresa},
          {path: '/GetEmpresa', name: 'GetEmpresa', component: GetEmpresa},
          {path: '/EditEmpresa/:id', name: 'EditEmpresa', component: EditEmpresa},


   
      






      ]
    },

  ]
});
