import Vue from 'vue';
import App from './App.vue';
import VueProgressBar from 'vue-progressbar';
import BlockUI from 'vue-blockui';
import router from './router';
import es from 'vee-validate/dist/locale/es';
import VeeValidate, { Validator } from 'vee-validate';
import VModal from 'vue-js-modal';
import VuePagination from 'vue-bs-pagination';
import * as uiv from 'uiv';
import axios from 'axios';
import Snotify, { SnotifyPosition } from 'vue-snotify';
import PrettyCheckbox from 'pretty-checkbox-vue';
import VueTabs from 'vue-nav-tabs'
import 'vue-nav-tabs/themes/vue-tabs.css'
import VueTheMask from 'vue-the-mask'
import VueCurrencyFilter from 'vue-currency-filter'
import VueDragscroll from 'vue-dragscroll'
Vue.use(VueDragscroll)


Vue.use(VueTheMask)
Vue.use(VueTabs)
Vue.use(VModal, { dialog: true });


Vue.use(PrettyCheckbox);
//Vue.use(VueCurrencyFilter)

Validator.localize('es', es);

Vue.use(BlockUI);


Vue.use(VueCurrencyFilter,
  {
    symbol : '$',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
  })

const options = {
  toast: {
    position: SnotifyPosition.rightTop
  }
}

  //  axios.defaults.baseURL = 'http://187.163.207.252:7000/SoftvWCFService.svc';
 axios.defaults.baseURL = 'http://localhost:64485/SoftvWCFService.svc'; 

Vue.use(Snotify, options);
const config = {
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'fields',
  delay: 0,
  locale: 'es',
  dictionary: null,
  strict: true,
  classes: false,
  classNames: {
    touched: 'touched', // the control has been blurred
    untouched: 'untouched', // the control hasn't been blurred
    valid: 'valid', // model is valid
    invalid: 'invalid', // model is invalid
    pristine: 'pristine', // control has not been interacted with
    dirty: 'dirty' // control has been interacted with
  },
  events: 'input|blur',
  inject: true,
  validity: false,
  aria: true,
  i18n: null, // the vue-i18n plugin instance,
  i18nRootKey: 'validations' // the nested key under which the validation messsages will be located
};

Vue.use(VeeValidate, config);

Vue.use(uiv);
Vue.component('VuePagination', VuePagination);

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
});
Vue.config.productionTip = false;
window.$ = window.jQuery = require('jquery');
new Vue({
  router,
  render: h => h(App)
}).$mount('#app');


